import GameEntity from './game-entity';

export default abstract class GameCharacter extends GameEntity {
	private speed: number;
	private direction: string | null;

	constructor(pos: [number,number], speed: number, direction: string | null = null) {
		super(pos);
		this.speed = speed;
		this.direction = direction;
	}

	public setSpeed(speed: number): void {
		this.speed = speed;
	}
	public getSpeed(): number {
		return this.speed;
	}
	public setDirection(direction: string): void {
		this.direction = direction;
	}
	public getDirection(): string {
		return this.direction;
	}
}