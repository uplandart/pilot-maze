export class GameEntityPosition {
	private x: number;
	private y: number;

	constructor(x: number, y: number) {
		this.x = x;
		this.y = y;
	}

	public setPos(x, y) {
		this.setPosX(x);
		this.setPosY(y);
	}
	public setPosX(pos) {
		this.x = pos;
	}
	public setPosY(pos) {
		this.y = pos;
	}
	public getPosX() {
		return this.x;
	}
	public getPosY() {
		return this.y;
	}
}

export default abstract class GameEntity {
	private pos: GameEntityPosition;

	constructor(pos: [number,number]) {
		this.pos = new GameEntityPosition(pos[0], pos[1]);
	}

	abstract update(dt: number): void;
	abstract render(ctx: CanvasRenderingContext2D): void;

	public setPos(x, y) {
		this.pos.setPos(x, y);
	}
	public setPosX(pos) {
		this.pos.setPosX(pos);
	}
	public setPosY(pos) {
		this.pos.setPosY(pos);
	}
	public getPosX() {
		return this.pos.getPosX();
	}
	public getPosY() {
		return this.pos.getPosY();
	}
}