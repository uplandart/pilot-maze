export default class GameHandlerInput {
	private pressedKeys: any;

	constructor() {
		this.pressedKeys = {};

		document.addEventListener('keydown', (e) => {
			this.setKey(e, true);
		});

		document.addEventListener('keyup', (e) => {
			this.setKey(e, false);
		});

		window.addEventListener('blur', () => {
			this.pressedKeys = {};
		});
	}

	public isDown(key: string) {
		return this.pressedKeys[key.toUpperCase()];
	}

	private setKey(event: KeyboardEvent, status: boolean) {
		let code = event.keyCode;
		let key;

		switch(code) {
			case 37:
				key = 'LEFT'; break;
			case 38:
				key = 'UP'; break;
			case 39:
				key = 'RIGHT'; break;
			case 40:
				key = 'DOWN'; break;
			default:
				// Convert ASCII codes to letters
				key = String.fromCharCode(code);
		}

		this.pressedKeys[key] = status;
	}
}