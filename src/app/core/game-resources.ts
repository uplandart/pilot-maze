export default class GameResources {
	private stack: Array<string>;
	private resourceCache: any;
	private readyCallbacks: Array<Function>;

	constructor() {
		this.stack = [];
		this.resourceCache = {};
		this.readyCallbacks = [];
	}

	public isReady(): boolean {
		let ready = true;
		for(let k in this.resourceCache) {
			if(this.resourceCache.hasOwnProperty(k) && !this.resourceCache[k]) {
				ready = false;
			}
		}
		return ready;
	}
	public addUrl(url: string): GameResources {
		this.stack.push(url);
		return this;
	}
	public addUrls(arr: Array<string>): GameResources {
		arr.forEach(url => {
			this.addUrl(url);
		});
		return this;
	}

	public onReady(func: Function): GameResources {
		this.readyCallbacks.push(func);
		return this;
	}
	public get(url): any {
		return this.resourceCache[url];
	}

	public load(): GameResources {
		if (!this.stack.length) {
			this.readyCallbacks.forEach(func => { func(); });
		} else {
			this.stack.forEach(url => {
				if (this.resourceCache[url]) {
					if (this.isReady()) {
						this.readyCallbacks.forEach(func => {
							func();
						});
					}
				} else {
					let img = new Image();
					this.resourceCache[url] = null;
					img.onload = () => {
						this.resourceCache[url] = img;

						if (this.isReady()) {
							this.readyCallbacks.forEach(func => {
								func();
							});
						}
					};
					this.resourceCache[url] = false;
					img.src = url;
				}
			});
		}
		return this;
	}
}