import RequestAnimationFrame from './request-animation-frame';
import GameHandlerInput from './game-handler-input';
import GameResources from './game-resources';
import GameEntity from "./game-entity";

export default abstract class Game {
	protected canvas: HTMLCanvasElement;
	protected ctx: CanvasRenderingContext2D;
	protected requestAnimationFrameFunction: Function;
	protected handlerInput: GameHandlerInput;
	private resources: GameResources;
	private lastTime: number;

	constructor(canvas: HTMLCanvasElement) {
		this.canvas = canvas;
		this.ctx = canvas.getContext('2d');
		this.requestAnimationFrameFunction = RequestAnimationFrame.getFunction();
		this.handlerInput = new GameHandlerInput();
		this.resources = new GameResources();
	}

	abstract reset(): void;
	abstract update(dt: number): void;
	abstract render(): void;

	public setRequestAnimationFrame(func: Function): void {
		this.requestAnimationFrameFunction = func;
	}
	public addResourcesUrl(url: string): void {
		this.resources.addUrl(url);
	}
	public addResourcesUrls(arr: Array<string>): void {
		this.resources.addUrls(arr);
	}
	public getHandlerInput(): GameHandlerInput {
		return this.handlerInput;
	}
	public getCanvas(): HTMLCanvasElement {
		return this.canvas;
	}
	public getCtx(): CanvasRenderingContext2D {
		return this.ctx;
	}
	public start(): void {
		console.log('Game start');
		this.resources
			.onReady(() => { this.init(); })
			.load();
	}

	protected handleInput(dt: number): void {

	}
	protected checkCollisions(): void {

	}
	protected renderEntity(entity: GameEntity): void {
		this.ctx.save();
		this.ctx.translate(entity.getPosX(), entity.getPosY());
		entity.render(this.ctx);
		// entity.sprite.render(ctx);
		this.ctx.restore();
	}
	protected renderEntities(entities: Array<GameEntity> | { [name: string]: GameEntity }): void {
		if (Array.isArray(entities)) {
			for (let i = 0; i < entities.length; i++) {
				this.renderEntity(entities[i]);
			}
		} else {
			for (let key in entities) {
				if (!entities.hasOwnProperty(key)) continue;
				this.renderEntity(entities[key]);
			}
		}
	}
	protected updateEntity(entity: GameEntity, dt: number): void {
		entity.update(dt);
	}
	protected updateEntities(entities: Array<GameEntity> | { [name: string]: GameEntity }, dt: number): void {
		if (Array.isArray(entities)) {
			for (let i = 0; i < entities.length; i++) {
				this.updateEntity(entities[i], dt);
			}
		} else {
			for (let key in entities) {
				if (!entities.hasOwnProperty(key)) continue;
				this.updateEntity(entities[key], dt);
			}
		}
	}

	private init(): void {
		this.reset();
		this.lastTime = Date.now();
		this.main();
	}
	private main(): void {
		let now = Date.now();
		let dt = (now - this.lastTime) / 1000.0;

		this.update(dt);
		this.render();

		this.lastTime = now;
		this.requestAnimationFrameFunction(() => { this.main(); });
	}
}