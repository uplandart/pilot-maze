export default class RequestAnimationFrame {
	static getFunction(): Function {
		let ref = window.requestAnimationFrame  ||
			window.webkitRequestAnimationFrame ||
			// window.mozRequestAnimationFrame    ||
			// window.oRequestAnimationFrame      ||
			// window.msRequestAnimationFrame     ||
			function (callback): void {
				window.setTimeout(callback, 1000 / 60);
			};
		return ref.bind(window);
	}
}