import GameCharacter from '../../core/game-character';

export default class PilotCharacter extends GameCharacter {
	private radius: number;

	constructor(pos: [number,number], speed: number, radius: number = 5) {
		super(pos, speed);
		this.radius = radius;
	}
	public getRadius(): number {
		return this.radius;
	}
	public setRadius(radius: number): void {
		this.radius = radius;
	}
	update(dt: number): void {
		let direction = this.getDirection();
		if (direction) {
			switch (direction) {
				case 'LEFT':
					this.setPosX(this.getPosX() - this.getSpeed() * dt);
					break;
				case 'UP':
					this.setPosY(this.getPosY() - this.getSpeed() * dt);
					break;
				case 'RIGHT':
					this.setPosX(this.getPosX() + this.getSpeed() * dt);
					break;
				case 'DOWN':
					this.setPosY(this.getPosY() + this.getSpeed() * dt);
					break;
			}
		}
	}
	render(ctx: CanvasRenderingContext2D): void {
		ctx.beginPath();
		ctx.arc(this.getPosX(), this.getPosY(), this.radius, 0, 2 * Math.PI, false);
		ctx.fillStyle = 'blue';
		ctx.fill();
		ctx.strokeStyle = 'black';
		ctx.stroke();
	}
}