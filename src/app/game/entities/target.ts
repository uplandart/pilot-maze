import GameEntity from '../../core/game-entity';

export default class TargetEntity extends GameEntity {
	private radius: number;

	constructor(pos: [number,number], radius: number = 7) {
		super(pos);
		this.radius = radius;
	}
	public getRadius(): number {
		return this.radius;
	}
	public setRadius(radius: number): void {
		this.radius = radius;
	}
	update(dt: number): void {
		throw new Error("Method not implemented.");
	}
	render(ctx: CanvasRenderingContext2D): void {
		ctx.beginPath();
		ctx.arc(this.getPosX(), this.getPosY(), this.radius, 0, 2 * Math.PI, false);
		ctx.fillStyle = 'green';
		ctx.fill();
		ctx.strokeStyle = 'black';
		ctx.stroke();
		ctx.closePath();
		ctx.beginPath();
		ctx.arc(this.getPosX(), this.getPosY(), this.radius - 3, 0, 2 * Math.PI, false);
		ctx.strokeStyle = 'black';
		ctx.stroke();
		ctx.closePath();
		ctx.beginPath();
		ctx.arc(this.getPosX(), this.getPosY(), 1, 0, 2 * Math.PI, false);
		ctx.fillStyle = 'black';
		ctx.fill();
	}
}