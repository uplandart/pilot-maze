import GameEntity, { GameEntityPosition } from '../../core/game-entity';

export default class WallEntity extends GameEntity {
	private coords: Array<GameEntityPosition>;

	constructor(pos: [number,number], coords: Array<[number,number]>) {
		super(pos);

		let resCoordsPositions = [];
		coords.forEach(coord => {
			resCoordsPositions.push(new GameEntityPosition(coord[0], coord[1]));
		});
		this.coords = resCoordsPositions;
	}
	update(dt: number): void {}
	render(ctx: CanvasRenderingContext2D): void {
		ctx.beginPath();
		ctx.moveTo(this.getPosX(), this.getPosY());
		for (let i = 0; i < this.coords.length; i++) {
			const coord = this.coords[i];
			ctx.lineTo(coord.getPosX(), coord.getPosY());
		}
		ctx.fillStyle = 'white';
		ctx.fill();
	}
}