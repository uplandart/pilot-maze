import Game from '../core/game';

import WallEntity from './entities/wall';
import TargetEntity from './entities/target';
import PilotCharacter from './characters/pilot';

export default class PilotMazeGame extends Game {
	private entityPilot: PilotCharacter;
	private entityWall: WallEntity;
	private entityTarget: TargetEntity;

	reset(): void {
		// Maze Example 1
		let wallMazeCoords = [];
		wallMazeCoords.push([50,  5]);
		wallMazeCoords.push([80,  60]);
		wallMazeCoords.push([120, 40]);
		wallMazeCoords.push([150, 40]);
		wallMazeCoords.push([150, 70]);
		wallMazeCoords.push([120, 70]);
		wallMazeCoords.push([70,  100]);
		wallMazeCoords.push([30,  30]);
		wallMazeCoords.push([5,   30]);
		// TODO: create first example maze like in real game

		this.entityWall   = new WallEntity([5, 5], wallMazeCoords);
		this.entityPilot  = new PilotCharacter([10, 12], 50, 5);
		this.entityTarget = new TargetEntity([70, 30], 7);
	}
	handleInput(dt: number): void {
		const handlerInput = this.handlerInput;

		if(handlerInput.isDown('DOWN') || handlerInput.isDown('s')) {
			this.entityPilot.setDirection('DOWN');
		}
		if(handlerInput.isDown('UP') || handlerInput.isDown('w')) {
			this.entityPilot.setDirection('UP');
		}
		if(handlerInput.isDown('LEFT') || handlerInput.isDown('a')) {
			this.entityPilot.setDirection('LEFT');
		}
		if(handlerInput.isDown('RIGHT') || handlerInput.isDown('d')) {
			this.entityPilot.setDirection('RIGHT');
		}
	}
	checkCollisions(): void {
		// TODO: check pilot collision maze
		// TODO: check pilot collision target
	}
	update(dt: number): void {
		this.handleInput(dt);

		this.updateEntity(this.entityWall, dt);
		this.updateEntity(this.entityPilot, dt);

		this.checkCollisions();
	}
	render(): void {
		this.ctx.fillStyle = 'red';
		this.ctx.fillRect(0, 0, this.canvas.width, this.canvas.height);

		this.renderEntity(this.entityWall);
		this.renderEntity(this.entityTarget);
		this.renderEntity(this.entityPilot);
	}
}