import PilotMazeGame from './game/index';

const canvasElement = <HTMLCanvasElement> document.getElementById("canvas");
const game = new PilotMazeGame(canvasElement);
game.start();