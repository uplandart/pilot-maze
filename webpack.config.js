'use strict';
const path = require('path');
const merge = require('webpack-merge');
const HtmlWebpackPlugin = require('html-webpack-plugin');

const devserver = require('./webpack/devserver');
const typescript = require('./webpack/typescript');
const pug = require('./webpack/pug');
const files = require('./webpack/files');
const extractCSS = require('./webpack/css.extract');
const uglifyJS = require('./webpack/js.uglify');

const PATHS = {
	source: path.join(__dirname, 'src'),
	build:  path.join(__dirname, 'dist')
};

const common = merge([
	{
		entry: path.join(PATHS.source, 'index.ts'),
		output: {
			path: PATHS.build,
			filename: './assets/js/[name].js'
		},
		resolve: {
			extensions: ['.ts','.js']
		},
		plugins: [
			new HtmlWebpackPlugin({
				filename: 'index.html',
				chunks: ['index'],
				template: path.join(PATHS.source, 'index.pug')
			})
		]
	},
	typescript(),
	pug(),
	extractCSS(),
	files(),
]);

module.exports = function (env) {
	if (env === 'production'){
		return merge([
			common,
			uglifyJS(),
		]);
	}
	if (env === 'development'){
		return merge([
			common,
			devserver(PATHS),
		])
	}
};