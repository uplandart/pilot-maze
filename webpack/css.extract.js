const ExtractTextPlugin = require('extract-text-webpack-plugin');

const BASE_CSS_LOADER = 'css-loader';
const CSS_LOADER_OPTIONS = { sourceMap: true, minimize: true };

module.exports = function (paths) {
	return {
		module: {
			rules: [
				{
					test: /\.styl$/,
					include: paths,
					use: ExtractTextPlugin.extract({
						publicPath: '../',
						fallback: 'style-loader',
						use: [
							{ loader: BASE_CSS_LOADER, options: CSS_LOADER_OPTIONS },
							{ loader: 'stylus-loader'},
						],
					}),
				},
				{
					test: /\.css$/,
					include: paths,
					use: ExtractTextPlugin.extract({
						fallback: 'style-loader',
						use: [
							{ loader: BASE_CSS_LOADER, options: CSS_LOADER_OPTIONS },
						],
					}),
				},
			],
		},
		plugins: [
			new ExtractTextPlugin({
				disable: false,
				filename: "./assets/css/[name].css",
				allChunks: true
			}),
		],
	}
};