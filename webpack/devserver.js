module.exports = function (PATHS) {
	return {
		devServer: {
			contentBase: PATHS.build,
			port: 8000
		}
	}
};